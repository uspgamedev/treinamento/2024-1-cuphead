extends CharacterBody2D

#melhor colocar const do que float, pq não vai mudar no decorrer do jogo
#se tivermos algum tipo de buff de movimento, aí compensa mudar
const speed = 300.0
const jump_force = -400.0

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	
	#Gravity
	if not is_on_floor():
		velocity.y += gravity * delta

	#Jump (Space)
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = jump_force
		
	#Directions (WASD ou Setinhas)
	var direction = 0
	if Input.is_action_pressed("ui_right"):
		direction += 1
	if Input.is_action_pressed("ui_left"):
		direction -= 1
	if direction:
		velocity.x = direction * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)

	move_and_slide()

	#Click do Mouse -> lembrando que se mudar o input, tem que mudar o parâmetro
	if Input.is_action_just_pressed("mouse_fire_1"):
		shoot()

	#Atirar(futuro)
func shoot():
	print("pew pew")
	pass
