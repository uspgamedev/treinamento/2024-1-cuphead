class_name Hurtbox

extends Area2D

func _on_area_entered(area):
	if area is Hitbox:
		owner.damage(area.damage)
