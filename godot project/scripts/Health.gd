extends Node

signal killed

@export var maxHealth = 3
@onready var currentHealth: int = maxHealth

func getHealth():
	return currentHealth

func damage(amount: int) -> void:
	currentHealth -= amount
	if currentHealth <= 0:
		killed.emit()

func heal(amount: int) -> void:
	if currentHealth + amount < maxHealth:
		currentHealth += amount
	else:
		currentHealth = maxHealth
