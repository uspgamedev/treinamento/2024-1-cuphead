extends Hitbox

var range = 0
var speed
var dir = Vector2()
const MAX = 1000

func constructor(damAmount: int, speAmount: int, direction: Vector2):
	damage = damAmount
	speed = speAmount
	dir = direction
	return self

func _physics_process(delta):
	position += dir * speed * delta
	
	range += speed * delta
	if range > MAX:
		queue_free()
