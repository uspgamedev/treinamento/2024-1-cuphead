extends Area2D

@export var bullet: PackedScene = preload("res://bullet.tscn")

enum bullet1 {damage = 1, speed = 100}
enum bullet2 {damage = 5, speed = 35}

func shoot(direction: Vector2):
	var newBullet = bullet.instantiate().constructor(bullet1.damage, bullet1.speed, direction)
	newBullet.global_position = $".".global_position
	newBullet.global_rotation = $".".global_rotation
	$".".add_child(newBullet)
